# CS371g: Generic Programming Integer Repo

* Name: Koshik Mahapatra

* EID: km47526

* GitLab ID: kmahapatra

* HackerRank ID: kmahapatra

* Git SHA: bcc5dff5ba2504b11998618c6bc555d2d4ef5b59

* GitLab Pipelines: https://gitlab.com/kmahapatra/cs371g-integer/-/pipelines

* Estimated completion time: 25

* Actual completion time: 20
