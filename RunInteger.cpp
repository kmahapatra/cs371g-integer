// --------------
// RunInteger.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline

#include "Integer.hpp"

int main () {
    std::string line;
    std::string a, op, b;
    while (std::getline(std::cin, line)) {
        std::istringstream str(line);
        str >> a;
        str >> op;
        str >> b;
        Integer<char> i(a);
        Integer<char> result = 0;
        if (op == "+") {
            Integer<char> j(b);
            result = i + j;
        } else if (op == "-") {
            Integer<char> j(b);
            result = i - j;
        } else if (op == "*") {
            Integer<char> j(b);
            result = i * j;
        } else if (op == "**") {
            result = pow(i, std::stoi(b));
        }
        std::cout << result << std::endl;
    }
    return 0;
}
