.DEFAULT_GOAL :=  all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
endif

# run docker
docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Integer.log:
	git log > Integer.log

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Integer code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Integer code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Integer.hpp
	-git add Integer.log
	-git add html
	git add makefile
	git add README.md
	git add RunInteger.cpp
	git add RunInteger.ctd
	git add RunInteger.in
	git add RunInteger.out
	git add TestInteger.cpp
	git commit -m "another commit"
	git push
	git status

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunInteger
	rm -f TestInteger

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Integer.log
	rm -f  Doxyfile
	rm -rf integer-tests
	rm -rf html
	rm -rf latex

# compile run harness
RunInteger: Integer.hpp RunInteger.cpp
	-$(CPPCHECK) RunInteger.cpp
	$(CXX) $(CXXFLAGS) RunInteger.cpp -o RunInteger

# compile test harness
TestInteger: Integer.hpp TestInteger.cpp
	-$(CPPCHECK) TestInteger.cpp
	$(CXX) $(CXXFLAGS) TestInteger.cpp -o TestInteger $(LDFLAGS)

# run/test files, compile with make all
FILES :=                                  \
    RunInteger                            \
    TestInteger

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	$(CHECKTESTDATA) RunInteger.ctd RunInteger.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunInteger.ctd RunInteger.tmp

# execute run harness and diff with expected output
run: ctd-check RunInteger
	./RunInteger < RunInteger.in > RunInteger.tmp
	-diff RunInteger.tmp RunInteger.out

# execute test harness
test: TestInteger
#	$(VALGRIND) ./TestInteger
	./TestInteger
	$(GCOV) TestInteger.cpp | grep -B 2 "cpp.gcov"

# test files in the Integer test repo
TFILES := `ls integer-tests/*.in`

# clone the Integer test repo
integer-tests:
	git clone https://gitlab.com/kmahapatra/cs371g-integer-tests.git integer-tests

# execute run harness against a test in Integer test repo and diff with expected output
integer-tests/%: RunInteger
	./RunInteger < $@.in > RunInteger.tmp
	diff RunInteger.tmp $@.out

# execute run harness against all tests in Integer test repo and diff with expected output
tests: integer-tests RunInteger
	-for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Integer.hpp
	$(ASTYLE) RunInteger.cpp
	$(ASTYLE) TestInteger.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Integer.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        \
    Integer.log                           \
    html

# uncomment the following line once you've pushed your test files
# you must replace GitLabID with your GitLabID
#    integer-tests/GitLabID-RunInteger

# check the existence of check files
check: $(CFILES)

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
