// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Integer.hpp"

using namespace std;

// ----
// plus
// ----

TEST(IntegerFixture, solve) {
    Integer<char> x = 25;
    Integer<char> y = 100;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -75);
}

TEST(IntegerFixture, solve2) {
    Integer<char> x = 2;
    Integer<char> y = -4;
    Integer<char> z = x + y;
    ASSERT_EQ(z, -2);
}

TEST(IntegerFixture, solve3) {
    Integer<char> x = 4;
    Integer<char> y = -2;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 2);
}

TEST(IntegerFixture, solve4) {
    Integer<char> x = -2;
    Integer<char> y = -4;
    Integer<char> z = x + y;
    ASSERT_EQ(z, -6);
}

TEST(IntegerFixture, solve5) {
    Integer<char> x = -100;
    Integer<char> y = 2;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -102);
}

TEST(IntegerFixture, solve6) {
    Integer<char> x = -10;
    Integer<char> y = -100;
    Integer<char> z = x + y;
    ASSERT_EQ(z, -110);
}

TEST(IntegerFixture, solve7) {
    Integer<char> x = -1000000;
    Integer<char> y = 100;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -1000100);
}

TEST(IntegerFixture, solve8) {
    Integer<char> x = -1000000;
    Integer<char> y = -100;
    Integer<char> z = x - y;
    ASSERT_EQ(z, -999900);
}

TEST(IntegerFixture, solve9) {
    Integer<char> x = 2;
    Integer<char> y = 7;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve10) {
    Integer<char> x = -7;
    Integer<char> y = -2;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve11) {
    Integer<char> x = -2;
    Integer<char> y = 2;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve12) {
    Integer<char> x = -25;
    Integer<char> y = -7;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve13) {
    Integer<char> x = -25;
    Integer<char> y = -2;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve14) {
    Integer<char> x = 2;
    Integer<char> y = 25;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve15) {
    Integer<char> x = 3;
    Integer<char> y = 10;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve16) {
    Integer<char> x = 7;
    Integer<char> y = 250;
    ASSERT_LT(x, y);
}

TEST(IntegerFixture, solve17) {
    Integer<char> x = 42;
    Integer<char> y = 50;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 2100);
}

TEST(IntegerFixture, solve18) {
    Integer<char> x = 10;
    Integer<char> y = -3;
    Integer<char> z = x * y;
    ASSERT_EQ(z, -30);
}

TEST(IntegerFixture, solve19) {
    Integer<char> x = 50;
    Integer<char> y = -7;
    Integer<char> z = x * y;
    ASSERT_EQ(z, -350);
}

TEST(IntegerFixture, solve20) {
    Integer<char> x = -10;
    Integer<char> y = -3;
    Integer<char> z = x * y;
    ASSERT_EQ(z, 30);
}

TEST(IntegerFixture, solve21) {
    Integer<char> x = 4;
    Integer<char> z = pow(x, 2);
    ASSERT_EQ(z, 16);
}

TEST(IntegerFixture, solve22) {
    Integer<char> x = 7;
    Integer<char> z = pow(x, 2);
    ASSERT_EQ(z, 49);
}

TEST(IntegerFixture, solve23) {
    Integer<char> x = -4;
    Integer<char> z = pow(x, 2);
    ASSERT_EQ(z, 16);
}

TEST(IntegerFixture, solve24) {
    Integer<char> x = -7;
    Integer<char> z = pow(x, 3);
    ASSERT_EQ(z, -343);
}
