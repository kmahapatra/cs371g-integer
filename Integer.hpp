// ---------
// Integer.h
// ---------

#ifndef Integer_h
#define Integer_h

// --------
// includes
// --------

#include <algorithm> // reverse
#include <cassert>   // assert
#include <cctype>    // isdigit
#include <iostream>  // ostream
#include <stdexcept> // invalid_argument
#include <string>    // string
#include <vector>    // vector

// -----------------
// shift_left_digits
// -----------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift left of the input sequence into the output sequence
 * ([b, e) << n) => x
 */
template <typename II, typename FI>
FI shift_left_digits (II b, II e, int n, FI x) {
    x = std::copy(b, e, x);
    for (; n > 0; --n, ++x) {
        *x = 0;
    }
    return x;
}

// ------------------
// shift_right_digits
// ------------------

/**
 * @param b an iterator to the beginning of an input  sequence (inclusive)
 * @param e an iterator to the end       of an input  sequence (exclusive)
 * @param x an iterator to the beginning of an output sequence (inclusive)
 * @return  an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the shift right of the input sequence into the output sequence
 * ([b, e) >> n) => x
 */
template <typename II, typename FI>
FI shift_right_digits (II b, II e, int n, FI x) {
    int total = std::distance(b, e);
    if (total <= n) {
        *x = 0;
        ++x;
    } else {
        for (int i = total - n; i > 0; --i, ++b, ++x) {
            *x = *b;
        }
    }
    return x;
}

// -----------
// plus_digits
// -----------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the sum of the two input sequences into the output sequence
 * ([b1, e1) + [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI plus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int n1 = std::distance(b1, e1);
    int n2 = std::distance(b2, e2);
    int carry = 0;
    int last_digit1 = 0; // for sign-extension
    int last_digit2 = 0; // for sign-extension
    int n = std::min(n1, n2);
    for(int i = 0; i < n; ++i, ++b1, ++b2, ++x) {
        last_digit1 = *b1;
        last_digit2 = *b2;
        int digit = last_digit1 + last_digit2 + carry;
        if (digit > 9) {
            carry = 1;
            digit -= 10;
        } else {
            carry = 0;
        }
        *x = digit;
    }
    if (n1 >= n2) {
        int sign_ext2 = last_digit2 >= 5 ? 9 : 0;
        for (; b1 != e1; ++b1, ++x) {
            int digit = *b1 + sign_ext2 + carry;
            if (digit > 9) {
                carry = 1;
                digit -= 10;
            } else {
                carry = 0;
            }
            *x = digit;
        }
    } else {
        int sign_ext1 = last_digit1 >= 5 ? 9 : 0;
        for (; b2 != e2; ++b2, ++x) {
            int digit = sign_ext1 + *b2 + carry;
            if (digit > 9) {
                carry = 1;
                digit -= 10;
            } else {
                carry = 0;
            }
            *x = digit;
        }
    }
    return x;
}

// ------------
// minus_digits
// ------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the difference of the two input sequences into the output sequence
 * ([b1, e1) - [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI minus_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int n1 = std::distance(b1, e1);
    int n2 = std::distance(b2, e2);
    int carry = 0;
    int carry2 = 1; // tens complement of b2 .. e2
    int last_digit1 = 0; // for sign extension
    int last_digit2 = 0; // for sign extension
    int n = std::min(n1, n2);
    for(int i = 0; i < n; ++i, ++b1, ++b2, ++x) {
        last_digit1 = *b1;
        last_digit2 = *b2;
        int digit2 = 9 - last_digit2 + carry2;
        if (digit2 > 9) {
            assert(carry2 == 1);
            digit2 -= 10;
        } else {
            carry2 = 0;
        }
        int digit = last_digit1 + digit2 + carry;
        if (digit > 9) {
            carry = 1;
            digit -= 10;
        } else {
            carry = 0;
        }
        *x = digit;
    }
    if (n1 >= n2) {
        int sign_ext2 = last_digit2 >= 5 ? 9 : 0;
        for (; b1 != e1; ++b1, ++x) {
            int digit2 = 9 - sign_ext2 + carry2;
            if (digit2 > 9) {
                assert(carry2 == 1);
                digit2 -= 10;
            } else {
                carry2 = 0;
            }
            int digit = *b1 + digit2 + carry;
            if (digit > 9) {
                carry = 1;
                digit -= 10;
            } else {
                carry = 0;
            }
            *x = digit;
        }
    } else {
        int sign_ext1 = last_digit1 >= 5 ? 9 : 0;
        for (; b2 != e2; ++b2, ++x) {
            int digit2 = 9 - *b2 + carry2;
            if (digit2 > 9) {
                assert(carry2 == 1);
                digit2 -= 10;
            } else {
                carry2 = 0;
            }
            int digit = sign_ext1 + digit2 + carry;
            if (digit > 9) {
                carry = 1;
                digit -= 10;
            } else {
                carry = 0;
            }
            *x = digit;
        }
    }
    return x;
}

// -----------------
// multiplies_digits
// -----------------

/**
 * @param b  an iterator to the beginning of an input  sequence (inclusive)
 * @param e  an iterator to the end       of an input  sequence (exclusive)
 * @param b2 an iterator to the beginning of an input  sequence (inclusive)
 * @param e2 an iterator to the end       of an input  sequence (exclusive)
 * @param x  an iterator to the beginning of an output sequence (inclusive)
 * @return   an iterator to the end       of an output sequence (exclusive)
 * the sequences are of decimal digits
 * output the product of the two input sequences into the output sequence
 * ([b1, e1) * [b2, e2)) => x
 */
template <typename II1, typename II2, typename FI>
FI multiplies_digits (II1 b1, II1 e1, II2 b2, II2 e2, FI x) {
    int n2 = std::distance(b2, e2);
    assert((n2 == 1 && *b2 < 5) || (n2 == 2 && *b2 >= 5 && *(b2+1) == 0));
    int carry = 0;
    int last_digit = 0;
    for (; b1 != e1; ++b1, ++x) {
        last_digit = *b1 * *b2 + carry;
        if (last_digit > 9) {
            carry = last_digit / 10;
            last_digit -= carry * 10;
        } else {
            carry = 0;
        }
        *x = last_digit;
    }
    if (carry > 0) {
        *x = last_digit = carry;
        ++x;
    }
    if (last_digit >= 5) {
        *x = 0;
        ++x;
    }
    return x;
}

// -------
// Integer
// -------

template <typename T, typename C = std::vector<T>>
class Integer {
    // -----------
    // operator ==
    // -----------

    /**
     * checks if two integers are equal
     */
    friend bool operator == (const Integer& lhs, const Integer& rhs) {
        return lhs._x == rhs._x;
    }

    // -----------
    // operator !=
    // -----------

    /**
     * check if two integers are not equal
     */
    friend bool operator != (const Integer& lhs, const Integer& rhs) {
        return !(lhs == rhs);
    }

    // ----------
    // operator <
    // ----------

    /**
     * check if lhs integer is less than rhs integer
     */
    friend bool operator < (const Integer& lhs, const Integer& rhs) {
        bool sign_lhs = lhs._x[0] >= 5;
        bool sign_rhs = rhs._x[0] >= 5;
        if (sign_lhs != sign_rhs) {
            return sign_lhs;
        }
        if (!sign_lhs) {
            if (lhs._x.size() > rhs._x.size()) {
                return false;
            }
            return lhs._x.size() < rhs._x.size() || lhs._x < rhs._x;
        }
        return -lhs > -rhs;
    }

    // -----------
    // operator <=
    // -----------

    /**
     * check if lhs integer is less than or equal to rhs integer
     */
    friend bool operator <= (const Integer& lhs, const Integer& rhs) {
        return !(rhs < lhs);
    }

    // ----------
    // operator >
    // ----------

    /**
     * check if lhs integer is greater than rhs integer
     */
    friend bool operator > (const Integer& lhs, const Integer& rhs) {
        return (rhs < lhs);
    }

    // -----------
    // operator >=
    // -----------

    /**
     * check if lhs integer is greater than or equal to rhs integer
     */
    friend bool operator >= (const Integer& lhs, const Integer& rhs) {
        return !(lhs < rhs);
    }

    // ----------
    // operator +
    // ----------

    /**
     * return sum of two Integers
     */
    friend Integer operator + (Integer lhs, const Integer& rhs) {
        return lhs += rhs;
    }

    // ----------
    // operator -
    // ----------

    /**
     * return difference of two Integers
     */
    friend Integer operator - (Integer lhs, const Integer& rhs) {
        return lhs -= rhs;
    }

    // ----------
    // operator *
    // ----------

    /**
     * change the value by multiplying by rhs
     */
    friend Integer operator * (Integer lhs, const Integer& rhs) {
        return lhs *= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * change the value by left shifting by 'rhs'
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator << (Integer lhs, int rhs) {
        return lhs <<= rhs;
    }

    // -----------
    // operator >>
    // -----------

    /**
     * change the value by right shifting by rhs
     * @throws invalid_argument if (rhs < 0)
     */
    friend Integer operator >> (Integer lhs, int rhs) {
        return lhs >>= rhs;
    }

    // -----------
    // operator <<
    // -----------

    /**
     * stream insert operator
     */
    friend std::ostream& operator << (std::ostream& lhs, const Integer& rhs) {
        if (rhs._x[0] >= 5) {
            lhs << "-" << -rhs;
        } else if (rhs._x.size() == 1) {
            lhs.put('0' + rhs._x[0]);
        } else {
            auto iter = rhs._x.cbegin();
            for (; iter != rhs._x.end() && *iter == 0; ++iter) {
                ;
            }
            for (; iter != rhs._x.end(); ++iter) {
                lhs.put('0' + *iter);
            }
        }
        return lhs;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * return absolute value of x as another Integer
     */
    friend Integer find_abs (Integer x) {
        return x.abs();
    }

    // ---
    // pow
    // ---

    /**
     * power
     * return x raised to the power e
     * @throws invalid_argument if ((x == 0) && (e == 0)) || (e < 0)
     */
    friend Integer pow (Integer x, int e) {
        return x.pow(e);
    }

private:
    // ----
    // data
    // ----

    C _x; // the backing container

private:
    // -----
    // valid
    // -----

    bool valid () const { // class invariant
        if (_x.empty())
            return false;
        if (std::any_of(_x.cbegin(), _x.cend(), [](int i) {
        return i < 0 || i > 9;
    })) {
            return false;
        }
        return true;
    }

    void remove_leading() {
        auto last = std::end(_x);
        --last; // move to most significant digit
        if (*last == 0 || *last == 9) {
            for (auto prev = last; prev != std::begin(_x); ) {
                --prev;
                if (*last == *prev) {
                    last = prev;
                } else {
                    if ((*last == 0 && *prev < 5) || (*last == 9 && *prev >= 5)) {
                        last = prev;
                    }
                    break;
                }
            }
        }
        _x.resize(++last - std::begin(_x));
    }

public:
    // ------------
    // constructors
    // ------------

    /**
     * construct integer out of long long value
     */
    Integer (long long value) {
        bool sign_bit = false;
        if (value < 0) {
            sign_bit = true;
            value = -value;
        }
        do {
            *std::back_inserter(_x) = value % 10;
            value /= 10;
        } while(value != 0);
        if (_x.back() >= 5)
            *std::back_inserter(_x) = 0;
        if (sign_bit) {
            int carry = 1;
            for (auto iter = _x.begin(); iter != _x.end(); ++iter) {
                *iter = 9 - *iter + carry;
                if (*iter > 9) {
                    *iter -= 10;
                    assert(carry == 1);
                } else {
                    carry = 0;
                }
            }
            assert(carry == 0);
        }
        std::reverse(std::begin(_x), std::end(_x));
        assert(valid());
    }

    /**
     * construct integer out of string with optional negative sign
     * @throws invalid_argument if value is not a valid representation of an Integer
     */
    explicit Integer (const std::string& value) {
        if (value.empty())
            throw std::invalid_argument("Bad integer string");
        int i = 0;
        bool sign_bit = false;
        if (value[0] == '-') {
            sign_bit = true;
            i++;
            if (value.size() == 1)
                throw std::invalid_argument("Bad negative integer string");
        }
        for (int j = value.size() - 1; j >= i; j--) {
            int c = value[j];
            if (c < '0' || c > '9')
                throw std::invalid_argument("Non-digit character");
            *std::back_inserter(_x) = c - '0';
        }
        assert(!_x.empty());
        if (_x.back() >= 5)
            *std::back_inserter(_x) = 0;
        if (sign_bit) {
            int carry = 1;
            for (auto iter = _x.begin(); iter != _x.end(); ++iter) {
                *iter = 9 - *iter + carry;
                if (*iter > 9) {
                    *iter -= 10;
                    assert(carry == 1);
                } else {
                    carry = 0;
                }
            }
            assert(carry == 0);
        }
        std::reverse(std::begin(_x), std::end(_x));
    }

    Integer             (const Integer&) = default;
    ~Integer            ()               = default;
    Integer& operator = (const Integer&) = default;

    // ----------
    // operator -
    // ----------

    /**
    * returns Integer with negated value of *this
    */
    Integer operator - () const {
        int carry = 1;
        Integer<T> val = *this;
        for (auto iter = val._x.rbegin(); iter != val._x.rend(); ++iter) {
            int digit = 9 - *iter + carry;
            if (digit > 9) {
                digit -= 10;
                assert(carry == 1);
            } else {
                carry = 0;
            }
            *iter = digit;
        }
        return val;
    }

    // -----------
    // operator ++
    // -----------

    /**
     * pre-increment *this
     */
    Integer& operator ++ () {
        *this += 1;
        return *this;
    }

    /**
     * post-increment *this but return old value before increment
     */
    Integer operator ++ (int) {
        Integer x = *this;
        ++(*this);
        return x;
    }

    // -----------
    // operator --
    // -----------

    /**
     * pre-decrement *this
     */
    Integer& operator -- () {
        *this -= 1;
        return *this;
    }

    /**
     * post-decrement *this but return old value before decrement
     */
    Integer operator -- (int) {
        Integer x = *this;
        --(*this);
        return x;
    }

    // -----------
    // operator +=
    // -----------

    /**
     * add rhs to *this
     */
    Integer& operator += (const Integer& rhs) {
        if (this == &rhs) {
            return *this += Integer<T>(rhs);
        }
        int sign_ext1 = _x[0] >= 5 ? 9 : 0;
        int sign2 = rhs._x[0] >= 5 ? 9 : 0;
        bool abs_lt = sign_ext1 != sign2 ? find_abs(*this) < find_abs(rhs) : false;
        std::reverse(std::begin(_x), std::end(_x));
        if (_x.size() <= rhs._x.size()) {
            _x.resize(rhs._x.size() + 1, (T) sign_ext1);
        }
        plus_digits(std::begin(_x), std::end(_x), std::crbegin(rhs._x), std::crend(rhs._x), std::begin(_x));
        remove_leading();
        if (sign_ext1 != sign2) {
            if (((sign2 == 0 && abs_lt) || (sign2 && !abs_lt)) && _x.back() >= 5) {
                *std::back_inserter(_x) = 0;
            }
        } else if (sign2 == 0 && _x.back() >= 5) {
            *std::back_inserter(_x) = 0;
        }
        std::reverse(std::begin(_x), std::end(_x));
        return *this;
    }

    // -----------
    // operator -=
    // -----------

    /**
     * subtract rhs from *this
     */
    Integer& operator -= (const Integer& rhs) {
        if (this == &rhs) {
            *this = 0;
            return *this;
        }
        int sign_ext1 = _x[0] >= 5 ? 9 : 0;
        int sign2 = rhs._x[0] >= 5 ? 9 : 0;
        bool abs_lt = sign_ext1 == sign2 ? find_abs(*this) < find_abs(rhs) : false;
        std::reverse(std::begin(_x), std::end(_x));
        if (_x.size() <= rhs._x.size()) {
            _x.resize(rhs._x.size() + 1, (T) sign_ext1);
        }
        minus_digits(std::begin(_x), std::end(_x), std::crbegin(rhs._x), std::crend(rhs._x), std::begin(_x));
        remove_leading();
        if (sign_ext1 == sign2) {
            if (((sign2 == 0 && !abs_lt) || (sign2 && abs_lt)) && _x.back() >= 5) {
                *std::back_inserter(_x) = 0;
            }
        } else if (sign2 && _x.back() >= 5) {
            *std::back_inserter(_x) = 0;
        }
        std::reverse(std::begin(_x), std::end(_x));
        return *this;
    }

    // -----------
    // operator *=
    // -----------

    /**
     * multiply *this by rhs
     */
    Integer& operator *= (const Integer& rhs) {
        bool sign1 = _x[0] >= 5;
        bool sign2 = rhs._x[0] >= 5;
        // copy before damaging '*this', as '&rhs' could be 'this'.
        Integer<T> rhs_copy = rhs;
        if (sign1) {
            abs();
        }
        if (sign2) {
            rhs_copy.abs();
        }
        if (*this < 10 || rhs_copy < 10) {
            if (*this < rhs_copy) {
                std::swap(_x, rhs_copy._x);
            }
            std::reverse(std::begin(_x), std::end(_x));
            std::reverse(std::begin(rhs_copy._x), std::end(rhs_copy._x));
            auto new_size = _x.size() + 2; // for carry and sign
            // first reserve instead of resize
            _x.reserve(new_size);
            auto e = std::end(_x); // save end position before resize
            _x.resize(new_size); // will not invalidate 'e'
            auto iter =  multiplies_digits(std::begin(_x), e,
                                           std::begin(rhs_copy._x), std::end(rhs_copy._x),
                                           std::begin(_x));
            _x.resize(iter - std::begin(_x));
            remove_leading();
            std::reverse(std::begin(_x), std::end(_x));
        } else {
            int m = std::min(_x.size(), rhs_copy._x.size());
            int m2 = m / 2;
            Integer<T> high1 = (*this >> m2);
            Integer<T> low1 = *this - (high1 << m2);
            Integer<T> high2 = rhs_copy >> m2;
            Integer<T> low2 = rhs_copy - (high2 << m2);
            auto z0 = low1 * low2;
            auto z2 = high1 * high2;
            auto z1 = (low1 + high1) * (low2 + high2);
            *this = (z2 << (m2*2)) + ((z1 - z2 - z0) << m2) + z0;
        }
        if (sign1 != sign2) {
            *this = -*this;
        }
        return *this;
    }

    // ------------
    // operator <<=
    // ------------

    /**
     * shift left by n decimal digits - multiplying by 10^n
     */
    Integer& operator <<= (int n) {
        assert(n >= 0);
        auto new_size = _x.size() + n;
        // first reserve instead of resize
        _x.reserve(new_size);
        auto e = std::end(_x); // save end position before resize
        _x.resize(new_size); // will not invalidate 'e'
        shift_left_digits(std::begin(_x), e, n, std::begin(_x));
        return *this;
    }

    // ------------
    // operator >>=
    // ------------

    /**
     * shift right by n decimal digits - dividing by 10^n
     */
    Integer& operator >>= (int n) {
        assert(n >= 0);
        auto new_size = std::max((int) _x.size() - n, 0);
        auto e = std::end(_x); // save end position before resize
        _x.resize(std::max(new_size, 1));
        shift_right_digits(std::begin(_x), e, n, std::begin(_x));
        return *this;
    }

    // ---
    // abs
    // ---

    /**
     * absolute value
     * change Integer value to its absolute value
     */
    Integer& abs () {
        if (_x[0] >= 5) {
            int carry = 1;
            for (auto iter = _x.rbegin(); iter != _x.rend(); ++iter) {
                int digit = 9 - *iter + carry;
                if (digit > 9) {
                    digit -= 10;
                    assert(carry == 1);
                } else {
                    carry = 0;
                }
                *iter = digit;
            }
        }
        return *this;
    }

    // ---
    // pow
    // ---

    /**
     * power
     * change integer value to its power 'e'
     * @throws invalid_argument if ((this == 0) && (e == 0)) or (e < 0)
     */
    Integer& pow (int e) {
        if ((*this == 0 && e == 0) || e < 0) {
            throw std::invalid_argument("pow argument");
        }
        if (e == 0) {
            *this = 1;
        } else {
            Integer y = 1;
            while (e > 1) {
                if (e % 2 == 0) {
                    *this *= *this;
                    e = e / 2;
                } else {
                    y *= *this;
                    *this *= *this;
                    e = (e - 1) / 2;
                }
            }
            *this *= y;
        }
        return *this;
    }
};

#endif // Integer_h
